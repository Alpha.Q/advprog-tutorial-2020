package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {

    @Override
    public Armor craftArmor() {
        MetalArmor metalArmor = new MetalArmor();
        return metalArmor;
    }

    @Override
    public Weapon craftWeapon() {
        ThousandJacker thousandJacker = new ThousandJacker();
        return thousandJacker;
    }

    @Override
    public Skill learnSkill() {
        ThousandYearsOfPain thousandYearsOfPain = new ThousandYearsOfPain();
        return thousandYearsOfPain;
    }
}
