package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {

    @Override
    public Armor craftArmor() {
        ShiningArmor shiningArmor = new ShiningArmor();
        return shiningArmor;
    }

    @Override
    public Weapon craftWeapon() {
        ShiningBuster shiningBuster = new ShiningBuster();
        return shiningBuster;
    }

    @Override
    public Skill learnSkill() {
        ShiningForce shiningForce = new ShiningForce();
        return shiningForce;
    }
}
