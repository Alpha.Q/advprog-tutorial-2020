package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import org.mockito.Mock;
import org.mockito.Mockito;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    @Mock
    private HolyGrail holyGrail;

    @Test
    public void testGetHolyWish(){
        Mockito.when(holyGrail.getHolyWish()).thenReturn(HolyWish.getInstance());
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
    }
}
